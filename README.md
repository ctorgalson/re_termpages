# RE Term Pages

This module provides tokens and other utility functions for use on
Drupal's taxonomy term pages. We use this partly to support Display
Suite, and partly so we don't always need to introduce Panels into the
equation.

## Hooks

The module implements the following hooks:

- `hook_permission()`
  Provides a permission for limiting who can administer this module,
  **However**, there is currently no settings page for this module so it
  seems this function is superfluous
- `template_preprocess_block()`
  This is used to replace tokens provided by this module in block titles.
- `hook_token_info()`
  Provides a token, "Term Name" for use in block titles
- `hook_tokens()`
  Provides replacements for the token defined in `hook_token_info()`

## Other functions

- `_re_termpages_is_term_page()`
  Returns boolean value according to whether or not the current page is
  a term page (based regex for term paths: `/taxonomy\/term\/\d+/`)
- `_re_termpages_render_term_list()`
  Returns the term list for the current term; useful for preprocess
  fields in Display Suite layouts

